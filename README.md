Project portfolio,

Pour ce projet j'ai commencé par choisir les couleurs principal, sombre, principalement noir et blanc, avec une touche de bleu pour les liens des projets réalisés.
J'ai principalement utilisé HTML et CSS ainsi qu'un peu de bootstrap pour la navbar et la responsive de la page d'accueil.

Il y a deux pages dans mon portfolio, une "À. propos" et une "projets":
Page "À. propos":
il y a une photo ainsi que mon nom, ma profession et une petite description.
Cela est suivi par quelques compétences que je maitrise.

Page "Projets"
Sur cette page, vous pouvez consulter mes projets réalisés jusqu'à présent, avec un lien vers le site réalisé.

dans les 2 pages se trouve un footer où l'on peut me contacter par mail, un lien pour aller sur mon gitlab et mon Linkedin.

Desktop :

![Maquette de la Première page du portfolio](media/maquetteAcceuil.png)
![Maquette de la deuxième page du portfolio](media/maquetteProject.png)

Mobile :

![Maquette de la Première page du portfolio](media/MaquetteMobileAcceuil.png)
![Maquette de la deuxième page du portfolio](media/maquetteMobileProjet.png)
